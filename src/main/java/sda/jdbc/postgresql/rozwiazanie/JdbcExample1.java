package sda.jdbc.postgresql.rozwiazanie;/*
 *   JDBC  Połączenie do bazy - Postgresql/mysql
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class JdbcExample1 {

	private static Logger log = LoggerFactory.getLogger(JdbcExample1.class);

	public static void main(String[] argv) {

		log.info("----Test polaczenia do bazy MySQL --------");

		Connection connection = null;

		try {
			// w moim wypadku serwer mysql rzucal jakis blad zwiazany z strefa czasowa,
			// rozwiazaniem problemu bylo ustawienie parametru serverTimezone w parametach polaczenia
			connection = DriverManager.getConnection(
					"jdbc:mysql://127.0.0.1:3306/sda?serverTimezone=UTC", "root",
					"1234");


			log.info("Udalo sie polaczyc z mysql :)");

			PreparedStatement statement = connection.prepareStatement("select imie,nazwisko from klient where imie=?");
			statement.setString(1, "Jacek");

			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				System.out.println("Imie: " + rs.getString(1) + ", nazwisko: " + rs.getString(2));
			}
			rs.close();
			statement.close();
		} catch (SQLException e) {

			log.info("Blad polaczenia do bazy danych", e);
			return;

		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.info("Blad zamykania polaczenia z baza", e);
				}
			}
		}
	}

}