package sda.jdbc.postgresql.rozwiazanie;

import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Slf4j
public class JdbcExample2 {

    public static void main(String[] args) {

        // prosze zwrocic uwage na sposob otwarcia polaczenia
        // wewnatrz bloku try...
        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://127.0.0.1:5432/sda", "postgres",
                "1234")) {

            connection.setAutoCommit(false);

            PreparedStatement pstKlient = connection.prepareStatement("INSERT INTO " +
                    "klient(imie,nazwisko,rok_urodzenia,plec,data_zalozenia)" +
                    "values (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

            pstKlient.setString(1, "Szymon");
            pstKlient.setString(2, "Dembek");
            pstKlient.setDate(3, Date.valueOf(LocalDate.of(1980, 1, 1)));
            pstKlient.setString(4, "M");
            pstKlient.setDate(5, Date.valueOf(LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault()).toLocalDate()));

            pstKlient.executeUpdate();

            ResultSet rsIds = pstKlient.getGeneratedKeys();
            if (rsIds.next()) {
                PreparedStatement pstOcena = connection.prepareStatement("insert into ocena_ksiazki(ksiazka_id, klient_id, ocena) values (?,?,?)");

                pstOcena.setLong(1, 10); // jakas dowolna ksiazka
                pstOcena.setLong(2, rsIds.getLong(1));
                pstOcena.setInt(3, 9);

                pstOcena.executeUpdate();

                pstOcena.close();
            }

            pstKlient.close();
            connection.commit();

            // w tym przykladzie nie ma jawnego zamkniecia polaczenia  z baza poprzez
            // connection.close()
            // gdyz polaczenie zostalo utworzone w tzw. bloku try-with-resource ktore
            // zapewnia automatyczne zamkniecie zadeklarowanego zasobu
        } catch (SQLException e) {
            log.error("Blad polaczenia do bazy danych: {}", e.getMessage(), e);
            return;

        }
    }
}