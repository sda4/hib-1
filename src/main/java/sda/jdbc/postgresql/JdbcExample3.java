package sda.jdbc.postgresql;

import lombok.extern.slf4j.Slf4j;
import org.postgresql.ds.PGPoolingDataSource;

import java.sql.Connection;
import java.sql.SQLException;

@Slf4j
public class JdbcExample3 {
    public static void main(String[] args) {
        PGPoolingDataSource connectionPool = new PGPoolingDataSource();
        connectionPool.setDataSourceName("custom-datasource");
        connectionPool.setServerName("127.0.0.1");
        connectionPool.setPortNumber(5432);
        connectionPool.setDatabaseName("sda");
        connectionPool.setUser("postgres");
        connectionPool.setPassword("1234");
        connectionPool.setInitialConnections(1);
        connectionPool.setMaxConnections(2);

        try {
            connectionPool.initialize();
            Connection c1 = connectionPool.getConnection();

            Connection c2 = connectionPool.getConnection();

            Connection c3 = connectionPool.getConnection();

            connectionPool.close();

            log.info("Skonczone");
        } catch (SQLException e) {
            log.error("Blad tworzenia puli polaczen: " + e.getMessage(), e);
        }
    }
}
