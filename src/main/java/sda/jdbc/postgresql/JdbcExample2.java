package sda.jdbc.postgresql;

import lombok.extern.slf4j.Slf4j;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Slf4j
public class JdbcExample2 {

	 public static void main(String[] args) {

		try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			log.error("Nie znaleziono drivera Postgresa");
			e.printStackTrace();
			return;

		}

		try (Connection connection = DriverManager.getConnection(
				"jdbc:postgresql://127.0.0.1:5432/sda", "postgres",
				"1234")) {
			
			connection.setAutoCommit(false);

			PreparedStatement pst = connection.prepareStatement("INSERT INTO " +
					"klient(imie,nazwisko,rok_urodzenia,plec,data_zalozenia)" +
					"values (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);

			pst.setString(1, "Szymon");
			pst.setString(2, "Dembek");
			pst.setDate(3, Date.valueOf(LocalDate.of(1980,1,1)));
			pst.setString(4, "M");
			pst.setDate(5, Date.valueOf(LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault()).toLocalDate()));

			pst.executeUpdate();

			pst.close();

			// TODO: stworzyc ocene dowolnej ksiazki przez nowo dodanego klienta
			// HINT: pst.getGeneratedKeys();


			connection.commit();

		} catch (SQLException e) {

			log.error("Blad polaczenia do bazy danych");
			e.printStackTrace();
			return;

		}
	}
}