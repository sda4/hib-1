package sda.jdbc.postgresql;/*
 *   JDBC  Połączenie do bazy - Postgresql/mysql
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcExample1 {

	private static Logger log = LoggerFactory.getLogger(JdbcExample1.class);

	public static void main(String[] argv) {

		log.info("----Test polaczenia do bazy Postgres --------");

		// TODO: przerobic na MySQL

		try {

			Class.forName("org.postgresql.Driver");

		} catch (ClassNotFoundException e) {

			log.info("Nie znaleziono drivera Postgresa", e);
			e.printStackTrace();
			return;


		}

		log.info("PostgreSQL JDBC Driver zarejestrowany");

		Connection connection = null;

		try {

			connection = DriverManager.getConnection(
					"jdbc:postgresql://127.0.0.1:5432/sda", "postgres",
					"1234");


			log.info("Udalo sie polaczyc z postgresem :)");

			// TODO: wydobyć i wyświetlić klientów o imieniu Jacek

//			Statement statement = connection.createStatement();
//
//			ResultSet rs = statement.executeQuery();
//			while (rs.next()) {
//				System.out.println(rs.getString());
//			}
//			rs.close();
//			statement.close();
		} catch (SQLException e) {

			log.info("Blad polaczenia do bazy danych", e);
			return;

		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException e) {
					log.info("Blad zamykania polaczenia z baza", e);
				}
			}
		}
	}

}